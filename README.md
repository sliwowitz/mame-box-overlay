# MAME box Overlay

Gentoo overlay with recent MAME packaged as crossdev-enabled sdlmame, and a GUI suitable for an arcade box.

Repository XML definition is at https://gitlab.com/sliwowitz/mame-box-overlay/-/raw/main/repositories.xml

## Packages and features

 * games-emulation/sdlmame
   * built form the original [MAME sources](https://www.mamedev.org/release.html)
   * LTO support through a USE flag
     * MAME build system has its own facilities
     * ensure these work in combination with the [GentooLTO overlay](https://github.com/InBetweenNames/gentooLTO)
   * crossdev compilation
     * the ebuild supports x86 and amd64 architectures
     * support cross-compilation on amd64 build host vor x86 target
  * games-emulation/attract
    * [Attract-Mode Emulator Frontend](http://attractmode.org/)
    * WIP to replace wahcade
  * games-emulation/wahcade
    * [Wah!Cade](http://www.anti-particle.com/wahcade.shtml) Frontend
    * Not developed anymore, written in Python 2

## This is a work in progress. 

 * Cross-compiling with LTO doesn't work yet. Some of the ongoing issues are tracked in a [Gentoo forum thread](https://forums.gentoo.org/viewtopic-p-8683336.html).
 * Only MAME arcade build is being tested. No MESS testing done as of now.

## Usage

Use you favourite tool to enable the overlay. The cool kids use `eselect-repository`, apparently:
```
eselect repository add mame-box-overlay git https://gitlab.com/sliwowitz/mame-box-overlay.git
emaint sync -a
```
On a machine with ssh keys for gitlab access, you can use:
```
eselect repository add mame-box-overlay git git@gitlab.com:sliwowitz/mame-box-overlay.git
emaint sync -a
```
