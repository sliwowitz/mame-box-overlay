# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs git-r3 autotools

DESCRIPTION="A graphical front-end for mame."
HOMEPAGE="http://attractmode.org"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	media-libs/libsfml
	media-libs/openal
	media-video/ffmpeg[fontconfig,openal]
	media-libs/fontconfig
	media-libs/freetype
	app-arch/libarchive
	media-libs/libjpeg-turbo
	media-libs/glu
"

case "${PV}" in
    # Head of master branch. This is a Gentoo convention.
    9999)
        EGIT_REPO_URI="https://github.com/mickelson/attract.git"
        ;;
    # Tagged version.
    *)
        EGIT_REPO_URI="https://github.com/mickelson/attract.git"
        TAG="v${PV}"
        REFS="refs/tags/${TAG}"
        ;;
esac

src_prepare(){
	eapply -p1 "${FILESDIR}/gcc11_vis_workaround.patch"
	eapply_user
}

