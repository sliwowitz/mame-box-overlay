# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6
PYTHON_COMPAT=( python2_7 )

inherit bzr eutils python-single-r1

DESCRIPTION="MAME frontend suited to cabinet use"
HOMEPAGE="https://launchpad.net/wahcade"
SRC_URI=""
EBZR_REPO_URI="lp:wahcade"
LICENSE="GPL-2"
SLOT="0"

KEYWORDS="~x86 ~amd64"

IUSE="joystick gstreamer"

# imaging instead of pillow???
DEPEND="${PYTHON_DEPS}
dev-util/glade[python]
dev-python/pygtk
dev-python/pycairo
dev-python/pygobject
dev-python/pillow
dev-python/chardet
joystick? ( dev-python/pygame )
gstreamer? ( dev-python/gst-python )"

RDEPEND="${DEPEND}"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

src_unpack() {
	bzr_src_unpack
}

src_prepare() {
	python_foreach_impl run_in_build_dir default
	eapply -p0 "${FILESDIR}/save_favs.patch"
	cd "${S}"
	sed -i 's#PREFIX=/usr/local#PREFIX=${D}usr/games#' install
	sed -i 's#/usr/share/applications#${D}usr/share/applications#' install
	sed -i 's#/usr/share/pixmaps#${D}usr/share/pixmaps#' install

	# workaround non-working sed in src_install
	sed -i 's#echo "cd ${DESTDIR}" >> $BINDIR/#echo "cd /usr/games/share/wahcade" >> $BINDIR/#' install
	eapply_user
}

src_install() {
	python_foreach_impl run_in_build_dir default
	install --directory ${D}usr/share/applications
	install --directory ${D}usr/share/pixmaps
	install --directory ${D}usr/games/bin
	cd "${S}"
	./install
	# doesn't work here, works on command line
	#cd "${D}usr/games/bin"
	#sed -i 's#${D}#/#' wahcade*
}

